$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 4000

    });
    $('#reservar').on('show.bs.modal', function (e) {
      console.log('El modal se está mostrando');

      $('#reservarBTN').removeClass('btn-outline-success');
      $('#reservarBTN').addClass('btn-primary');
      $('#reservarBTN').prop('disabled', true);
    });
    $('#reservar').on('shown.bs.modal', function (e) {
      console.log('El modal se mostró');
    });

  });